## THIS PROJECT IS A BASH EXERCISE FOR THE FOLLOWING TASKS:

- get 2 positional arguments: `name` `count` and any number of additional arguments 
   #### For more info about positional parameters check [this](https://wiki.bash-hackers.org/scripting/posparams) out. 
- check that all additional arguments are non-numeric strings (fails if not)
- create a directory $HOME/mylogs
- create a file called `name`.log in the directory
- for `count` times iterate over the list of additional arguments and write a line with the argument string and a random word out of the following list : 
  - normal 
  - request 
  - failure
  - error
  - warning
  - alert
  - notice
  - note
- generate 10 log files using this utility
- write a log processing script that:
  - scans each file in $HOME/mylogs directory. For each file:
    - if lines with 'error' and 'failure' constitute more then 10% of all lines, log is unhealthy
    - if lines with 'error' and 'failure' constitute less then 10%, but lines with 'warning' and 'alert' are more then 20%, log is unstable
    - rest of the logs are considured healthy
  - zips the logs and copy them accordingly to subfolders:
    - $HOME/mylogs/unhealthy
    - $HOME/mylogs/unstable
    - $HOME/mylogs/healthy
  - Prints out a report coloring the output accordingly, e.g.: 
    - healthy | 5 log files | ab.log, course.log, devops.log, good.log, fine.log
    - unstable | 2 log files | ocean.log, earthquake.log
    - unhealthy | 3 log files | random.log, 23.log, balbala.log
