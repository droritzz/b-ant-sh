#!/usr/bin/env bash

#----------------- Safe header start -------------------------------------

# any failed command will cause the script to exit
set -o errexit

# exit if using undeclared variables
set -o nounset

#-------------------------------------------------------------------------


RED='\033[0;31m'
PURPLE='\033[1;35m'          
GREEN='\033[0;32m'

mylogs_dir="$HOME/mylogs"
log_files=( $(ls $mylogs_dir/*.log) )

function main (){
    create_dirs_zipped_logs
    declare_log_types 
    catalog_log_files
    print_report
}

function create_dirs_zipped_logs (){
    zipped_dirs=("unhealthy" "unstable" "healthy")
    for dir in "${zipped_dirs[@]}"; do
        if [ ! -d $mylogs_dir/$dir ]; then
            mkdir $mylogs_dir/$dir
        fi
    done
}

function declare_log_types (){
    unhealthy=()
    unstable=()
    healthy=()
}

function catalog_log_files (){

    for log_file in "${log_files[@]}"; do
      number_of_lines=$(cat $log_file | wc -l)

      number_of_errors=$(grep -o 'error' $log_file | wc -l)
      number_of_failures=$(grep -o 'failure' $log_file | wc -l)

      number_for_unhealthy=$(($number_of_errors + $number_of_failures))
      percentage_error_failure=$(echo 100*$number_for_unhealthy/$number_of_lines | bc -l)

      number_of_warnings=$(grep -o 'warning' $log_file | wc -l)
      number_of_alerts=$(grep -o 'alert' $log_file | wc -l)
      
      number_for_unstable=$(($number_of_warnings + $number_of_alerts))
      percentage_warning_alert=$(echo 100*$number_for_unstable/$number_of_lines | bc -l)

      log_name=$(echo $log_file | awk -F"/" '{print $5}')

      if [ ${percentage_error_failure%.*} -ge 10 ]; then
        unhealthy+=("$log_name")
      elif [ ${percentage_warning_alert%.*} -ge 20 ]; then
        unstable+=("$log_name")
      else
        healthy+=("$log_name")
      fi
    done
}

function print_report (){
    echo -e "${PURPLE}unstable | ${#unstable[@]} log files | ${unstable[*]}"
    echo -e "${RED}unhealthy | ${#unhealthy[@]} log files | ${unhealthy[*]}"
    echo -e "${GREEN}healthy | ${#healthy[@]} log files | ${unhealthy[*]}"
}

#--------------- Main function --------------
main
