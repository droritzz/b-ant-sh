#!/usr/bin/env bash

#----------------- Safe header start -------------------------------------

# any failed command will cause the script to exit
set -o errexit

# exit if using undeclared variables
set -o nounset

#-------------------------------------------------------------------------

name=$1
count=$2
shift 2
additional_args=$@

function main(){
        check_all_args_non_numeric
        create_logs_dir
        create_log_file
        iterate_over_additional_arguments
}

function check_all_args_non_numeric (){
        for arg in $additional_args; do
            regex='^-?[0-9]+([.][0-9]+)?$'
            if [[ $arg =~ $regex ]]; then
              echo "ERROR - one or more arguments has a numeric value, exiting"
              return 1
            fi
        done
}

function create_logs_dir (){
        log_dir="$HOME/mylogs"
        if [ ! -d $log_dir ]; then
                mkdir $log_dir
        fi
}

function create_log_file (){
        log_file=$name.log
        if create_logs_dir; then
                if [ ! -f $log_file ]; then
                        touch $log_dir/$log_file
                fi
        fi
}

function iterate_over_additional_arguments (){
        random_words=("normal" "request" "failure" "error" "warning" "alert" "notice" "note")
        random_count=0
        while [ $random_count -lt $count ]; do
            for arg in $additional_args; do
                echo "$arg ${random_words[$((RANDOM%7))]}" >> $log_dir/$log_file
            done
            let "random_count+=1"
        done
}

#--------------- Main function --------------
main
